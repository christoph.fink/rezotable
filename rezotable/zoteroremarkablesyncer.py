#!/usr/bin/env python3

"""ZoteroRemarkableSyncer: Sync files between a Zotero library and a ReMarkable e-reader, bidirectionally."""

import datetime
import os
import os.path
import string
import sys
import unicodedata

from .remarkabledocumentlister import RemarkableDocumentLister
from .zoteroattachmentlister import ZoteroAttachmentLister


class ZoteroRemarkableSyncer:
    """Sync files between a Zotero library and a ReMarkable e-reader, bidirectionally."""

    def __init__(
            self,
            zotero_api_key: str,
            zotero_user_id: str,
            zotero_storage_path: str = os.path.expanduser("~/Zotero/storage"),
            remarkable_literature_path: str = "/Literature"
    ):
        """Initialise a ZoteroRemarkableSyncer."""
        self.remarkable_document_lister = RemarkableDocumentLister()
        self.zotero_attachment_lister = ZoteroAttachmentLister(zotero_api_key, zotero_user_id, zotero_storage_path)

        self.REMARKABLE_LITERATURE_PATH = remarkable_literature_path
        self.ZOTERO_STORAGE_PATH = zotero_storage_path

    def sync(self):
        """Sync Zotero library and ReMarkable cloud."""
        max_count = 50  # debug: only process max_count attachments
        for zotero_attachment in self.zotero_attachment_lister.attachments:
            if not max_count:
                break
            try:
                remarkable_path = self._remarkable_path_for_zotero_attachment(zotero_attachment)
                zotero_path = self._zotero_path_for_zotero_attachment(zotero_attachment)
            except KeyError as exception:
                print(
                    exception,
                    zotero_attachment["key"],
                    file=sys.stderr
                )
                break  # only for DEBUGging

            if not zotero_path:
                continue

            try:
                zotero_mtime = datetime.datetime.fromtimestamp(
                    zotero_attachment["data"]["mtime"] / 1000,
                    tz=datetime.timezone.utc
                )
            except TypeError:
                zotero_mtime = datetime.datetime.fromtimestamp(0, tz=datetime.timezone.utc)

            if remarkable_path in self.remarkable_document_lister.documents_by_path:
                remarkable_document = (
                    self.remarkable_document_lister.documents_by_path[remarkable_path]
                )
                if zotero_mtime > remarkable_document.mtime:
                    print(
                        "zot->rm",
                        remarkable_document.name,
                        zotero_attachment["key"],
                        zotero_mtime,
                        remarkable_document.mtime,
                        (zotero_mtime - remarkable_document.mtime),
                        file=sys.stderr
                    )
                    self.remarkable_document_lister.update_document_contents(
                        remarkable_document,
                        open(zotero_path, "rb"),
                        zotero_mtime
                    )
                elif zotero_mtime < remarkable_document.mtime:
                    print(
                        "rm->zot (skipped)",
                        remarkable_document,
                        zotero_attachment["key"],
                        zotero_mtime,
                        remarkable_document.mtime,
                        (zotero_mtime - remarkable_document.mtime),
                        file=sys.stderr
                    )
                    continue
                    self.zotero_attachment_lister.update_attachment_contents(
                        zotero_attachment,
                        remarkable_document.annotated_s(),
                        remarkable_document.mtime
                    )
                else:
                    print(
                        "==",
                        remarkable_document.name,
                        zotero_attachment["key"],
                        zotero_mtime,
                        remarkable_document.mtime,
                        file=sys.stderr
                    )
            else:  # new file for remarkable
                print(
                    "NEW",
                    remarkable_path,
                    zotero_attachment["key"],
                    file=sys.stderr
                )
                self.remarkable_document_lister.create_new_document(
                    remarkable_path,
                    open(zotero_path, "rb"),
                    zotero_mtime
                )

    def _remarkable_path_for_zotero_attachment(self, zotero_attachment: dict) -> str:
        zotero_parent = self.zotero_attachment_lister.contributions[
            zotero_attachment["data"]["parentItem"]
        ]

        try:
            author = zotero_parent["data"]["creators"][0]
            try:
                author = author["lastName"]
            except KeyError:
                author = author["name"]
            author = unicodedata.normalize("NFD", author)
        except IndexError:  # No `contribution["data"]["creators"]`s
            author = "N.N."

        sort_key = "_"
        for character in author:
            if character in (string.ascii_letters + string.digits):
                sort_key = character.upper()
                break

        if zotero_parent["data"]["itemType"] == "statute":
            title = "{} {}".format(
                zotero_parent["data"]["nameOfAct"],
                zotero_parent["data"]["shortTitle"]
            )
        else:
            title = zotero_parent["data"]["title"]

        try:
            year = zotero_parent["meta"]["parsedDate"].split("-")[0]
        except KeyError:
            year = "n.d."

        remarkable_path = os.path.join(
            self.REMARKABLE_LITERATURE_PATH,
            sort_key,
            author,
            year,
            "{title:s} ({attachment_key:s})".format(
                title=title,
                attachment_key=zotero_attachment["key"]
            )
        )
        return remarkable_path

    def _zotero_path_for_zotero_attachment(self, zotero_attachment: dict) -> str:
        try:
            zotero_path = os.path.join(
                self.ZOTERO_STORAGE_PATH,
                zotero_attachment["key"],
                zotero_attachment["data"]["filename"]
            )
        except KeyError:
            zotero_path = ""
        return zotero_path
