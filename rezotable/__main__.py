#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#   Copyright (C) 2021 Christoph Fink
#
#   This program is free software; you can redistribute it and/or
#   modify it under the terms of the GNU General Public License
#   as published by the Free Software Foundation; either version 3
#   of the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, see <http://www.gnu.org/licenses/>.


"""rezotable: Sync files between a Zotero library and a ReMarkable e-reader, bidirectionally."""


import os
import os.path

import configargparse

from .zoteroremarkablesyncer import ZoteroRemarkableSyncer


def main():
    """Sync files between a Zotero library and a ReMarkable e-reader, bidirectionally."""
    argparser = configargparse.ArgParser(
        prog=__package__,
        description="Sync files between a Zotero library and a ReMarkable e-reader, bidirectionally.",
        default_config_files=[
            "/etc/{:s}.yml".format(__package__),
            os.path.join(
                (
                    os.environ.get("APPDATA")
                    or os.environ.get("XDG_CONFIG_HOME")
                    or os.path.join(os.environ["HOME"], ".config")
                ),
                "{:s}.yml".format(__package__)
            )
        ]
    )
    argparser.add(
        "-u",
        "--zotero-user-id",
        required=True,
        help="Zotero User ID (numeric, see https://www.zotero.org/settings/keys )",
        type=int
    )
    argparser.add(
        "-k",
        "--zotero-api-key",
        required=True,
        help="Zotero API key (see https://www.zotero.org/settings/keys )",
        type=str
    )
    argparser.add(
        "-z",
        "--zotero-storage-path",
        help="Path to Zotero’s local storage",
        type=str,
        default=os.path.expanduser("~/Zotero/storage")
    )
    argparser.add(
        "-r",
        "--remarkable-literature-folder",
        help="Folder on the ReMarkable to which to sync the Zotero library",
        type=str,
        default="/Literature"
    )

    options = argparser.parse_args()

    zotero_remarkable_syncer = ZoteroRemarkableSyncer(
        zotero_api_key=options.zotero_api_key,
        zotero_user_id=options.zotero_user_id,
        zotero_storage_path=options.zotero_storage_path,
        remarkable_literature_path=options.remarkable_literature_folder
    )
    zotero_remarkable_syncer.sync()


if __name__ == "__main__":
    main()
