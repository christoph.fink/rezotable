#!/usr/bin/env python3

"""ZoteroAttachmentLister: List all attachment in a Zotero storage."""

import datetime
import os
import os.path
import typing

import pyzotero.zotero


class ZoteroAttachmentLister:
    """List all attachment in a Zotero storage."""

    def __init__(self, zotero_api_key: str, zotero_user_id: str, zotero_storage_path: str) -> None:
        """Initialise a ZoteroAttachmentLister."""
        self.ZOTERO_STORAGE = zotero_storage_path
        self.api = pyzotero.zotero.Zotero(zotero_user_id, "user", zotero_api_key)
        self._attachments = {}
        self._contributions = {}

    def update_attachment_contents(
            self,
            attachment: dict,
            contents: typing.BinaryIO,
            mtime: datetime.datetime
    ) -> None:
        """Update the contents of a (file) attachment."""
        destination_file_path = os.path.join(
            self.ZOTERO_STORAGE,
            attachment["key"],
            attachment["data"]["filename"]
        )

        # overwrite file in zotero folder with new data
        with open(destination_file_path, "wb") as destination_file:
            destination_file.write(contents.read())

        # update the modification time stamp of that file
        mtime = mtime.timestamp()
        os.utime(destination_file_path, (mtime, mtime))

        # update the zotero library metadata record
        attachment["mtime"] = mtime * 1000  # zotero api expects milliseconds
        attachment["data"]["mtime"] = attachment["mtime"]
        self.api.update_item(attachment["data"])

    @property
    def attachments(self) -> dict:
        """List attachments with zotero and remarkable pathnames."""
        if not self._attachments:
            attachments = self.api.everything(self.api.items(itemType="attachment"))
            self._attachments = [
                attachment
                for attachment in attachments
                if "parentItem" in attachment["data"]
                and attachment["data"]["contentType"] == "application/pdf"
            ]
        return self._attachments

    @property
    def contributions(self) -> dict:
        """List all items in Zotero."""
        if not self._contributions:
            contributions = self.api.everything(
                self.api.items(
                    itemType=" || ".join(
                        [
                            "artwork",
                            # "attachment",
                            "audioRecording",
                            "bill",
                            "blogPost",
                            "book",
                            "bookSection",
                            "case",
                            "computerProgram",
                            "conferencePaper",
                            "dictionaryEntry",
                            "document",
                            "email",
                            "encyclopediaArticle",
                            "film",
                            "forumPost",
                            "hearing",
                            "instantMessage",
                            "interview",
                            "journalArticle",
                            "letter",
                            "magazineArticle",
                            "manuscript",
                            "map",
                            "newspaperArticle",
                            "note",
                            "patent",
                            "podcast",
                            "presentation",
                            "radioBroadcast",
                            "report",
                            "statute",
                            "thesis",
                            "tvBroadcast",
                            "videoRecording",
                            "webpage"
                        ]
                    )
                )
            )
            self._contributions = {
                contribution["key"]: contribution for contribution in contributions
            }
        return self._contributions
